import Vue from 'vue'
import VueRouter from 'vue-router'
// import Index from './views/Index.vue'
// import Login from './views/Logni.vue'

Vue.use(VueRouter)

const routes = [
  {
    path: '/',
    // name: 'index', //当前路由下有一个子路由，就不能给其设置name属性
    component: () => import("./views/Index.vue"), // 按需加载
    children:[
      {
        path: '',
        redirect: '/home' // 重定向：默认进入到home页面中
      },
      {
        path: "/home",
        name: "home",
        component: () => import("./views/Home.vue")
      },
      {
        path: "/order",
        name: "order",
        component: () => import("./views/Order.vue")
      },
      {
        path: "/me",
        name: "me",
        component: () => import("./views/Me.vue")
      },
      {
        path: "/address",
        name: "address",
        component: () => import("./views/Address.vue")
      },
      {
        path: "/city",
        name: "city",
        component: () => import("./views/City.vue")
      },
      {
        path: "/search",
        name: "search",
        component: () => import("./views/Search.vue")
      },
      {
        path: "/shop",
        name: "shop",
        component: () => import("./views/Shops/Shop.vue")
      },
    ]
  },
  {
    path: '/login',
    name: 'login',
    // component: Login
    component: () => import("./views/Login.vue")
  }
]

const router = new VueRouter({
  mode: 'history',
  base: process.env.BASE_URL,
  routes
})

// 路由守卫
router.beforeEach((to, from, next)=>{
  const isLogin = localStorage.ele_login ? true : false;
  if(to.path == "/login"){
    next(); //如果登录了就让它正常跳转
  }else{
    // 是否在登录状态下，在的话就让他正常跳转，否则就返回到login页面
    isLogin ? next() :next("/login")
  }
})

export default router
