import Vue from 'vue'
import App from './App.vue'
import MintUI from 'mint-ui'
import 'mint-ui/lib/style.css'
import router from './router.js'
import store from './store.js'
import axios from 'axios'
import 'font-awesome/css/font-awesome.min.css'

import { Indicator } from 'mint-ui';

Vue.config.productionTip = false
Vue.prototype.$axios = axios

Vue.use(MintUI)
// 请求拦截：axios请求的时候开启加载动画
axios.interceptors.request.use(
  config => {
    Indicator.open('加载中...');
    return config;
  },
  error => {
    return Promise.reject(error);
  }
)
// 响应拦截：响应的时候关闭
axios.interceptors.response.use(
  response => {
    Indicator.close();
    return response;
  },
  error => {
    Indicator.close();
    return Promise.reject(error);
  }
)



// axios.defaults.baseURL = 'https://c.iwanmen.com/element/api/'
new Vue({
  router,
  store,
  render: h => h(App)
}).$mount('#app')
